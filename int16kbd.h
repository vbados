#ifndef INT16KBD_H
#define INT16KBD_H

static bool int16_store_keystroke(uint8_t scancode, uint8_t character);
#pragma aux int16_store_keystroke = \
	"mov ah, 0x05" \
	"int 0x16" \
	__parm [ch] [cl] \
	__value [al] \
	__modify [ax]

#endif // INT16KBD_H
