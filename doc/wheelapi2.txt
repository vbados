This document describes an extension to the CuteMouse wheelapi.txt
to support two wheels (vertical and horizontal).

NOTE: Draft, subject to change.

---------------------------------------------------------------------
Changes to the wheelapi v1 functions:

INT 33/0011 - Check wheel support and get capabilities flags
    AX = 0011h
Return:	AX = 574Dh ('WM' in assembly) if Wheel API is supported by driver
    CX = Capabilities flag
        Bit(s)	Description
        ------	-----------
         0      1=Pointing device supports wheel
         1      1=Pointing device supports 2nd wheel
         2-15	Reserved
Notes:  this function should be examined before accessing wheel features.
        vbmouse currently assumes this is called after each int33/0, otherwise
          wheel events may be sent as keystrokes instead of wheelapi.

INT 33/0003 - Get cursor position, buttons status and wheel counter
    AX = 0003h
Return:	BL = buttons status (bits 0-4)
    CX = column
    DX = row
    BH = 8-bit signed counter of wheel movement since last call.
          positive value means downward wheel movement.
    AH = 8-bit signed counter of 2nd wheel movement since last call.
          positive value means rightward wheel movement.
Note:   calling this clears the wheel counter for ALL wheels.

INT 33/0005 - Get button press or wheel movement data
    AX = 0005h
    BX = button number, -1 for vertical wheel, -2 for 2nd/horizontal wheel
    (return values remain the same)
Note:   as in wheelapi v1, AH always contains (1st) wheel movement on return
          independently of button number requested in BX.

INT 33/0006 - Get button release or wheel movement data
    AX = 0006h
    BX = button number, -1 for vertical wheel, -2 for 2nd/horizontal wheel
    (return values remain the same)

INT 33/000C - Define User Interrupt Routine
INT 33/0014 - Exchange User Interrupt Routines
Bitfields for mouse call mask:
Bit(s)	Description
 0-6	same as Table 03171 (as in int33.lst)
 7      vertical wheel movement (as in wheelapi.txt)
 8      (absolute mouse event bit)
 9      horizontal wheel movement
 10     4th button pressed
 11     4th button released
 12     5th button pressed
 13     5th button released
 14-15	unused
Notes:	on entry, bit 9 of CX (call mask) indicates that the user routine
          will be called on horizontal wheel movement
        if the user routine is called with bit 9 of AX (condition mask) set,
          then BH will hold the 8-bit signed counter of HORIZONTAL wheel
          movement since the last call to the routine. if bit 7 is set, then
          BH holds VERTICAL wheel movement.
        it is impossible for the user routine to be called with both
          vertical (bit 7) and horizontal (bit 9) movement.
        A program that just sets 0xFFFF event mask and expects to find vertical
           wheel movement info in BH (wheelapi v1 style) will be confused, as
           here when bit 9 is set BH will contain horizontal wheel movement
           instead. DN/2 does this, so horizontal scrolling acts like vertical.
